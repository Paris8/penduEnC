# Pendu
Mon premier pendu tout language confondu, s'il y a des approximations tu peux faire une PR 😀

Pour lancer le pendu : `git clone https://gitlab.com/Mylloon/penduEnC.git && cd penduEnC && gcc main.c -o main && ./main listeMots.txt` devrait fonctionner.

Tu peux aussi télécharger l'artéfact de la [dernière pipeline](https://gitlab.com/Mylloon/penduEnC/-/pipelines/).
