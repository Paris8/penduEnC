#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int longueurMot(const char mot[]) { // pour connaitre la longueur d'un mot
    int taille;
    for(taille = 0; mot[taille] != 0; taille++);

    return taille;
}

char * recuperationMot(const char file[]) { // pour récupérer un mot aléaotoirement dans un fichier
    int tailleMaxMot = 25;
    char * mot = malloc(tailleMaxMot * sizeof(char));

    // gestion erreur
    FILE * fp = fopen(file, "r");
    if(fp == NULL) {
        perror("Error");
    }

    long int tailleFichier = 0;
    // on récupère le nombre de mots qu'il y a dans le fichier
    while(fgets(mot, tailleMaxMot, fp) != NULL) tailleFichier++;

    // chiffre aléatoire pour mot aléatoire
    srand(time(NULL));
    int chiffreAleatoire = (rand() % tailleFichier) + 1;

    // recuperation du mot sélectionner
    tailleFichier = 0;
    rewind(fp); // reset le pointeur au début du fichier
    while(fgets(mot, tailleMaxMot, fp) != NULL) {
        tailleFichier++;
        if(tailleFichier == chiffreAleatoire) break;
    }

    // fermeture du fichier
    fclose(fp);

    // suppression du saut de ligne à la fin du mot
    int tailleMot = longueurMot(mot);
    if(mot[tailleMot - 1] == '\n') mot[tailleMot - 1] = 0;

    // passage en majusucule du mot
    for(int i = 0; i < tailleMot; i++) if(mot[i] >= 97 && mot[i] <= 122) mot[i] = mot[i] - 32;

    return mot;
}

int caractereSpecial(char lettre) { // vérification que la lettre n'est pas un caractere special
    if(lettre == ' ') return 1;
    if(lettre == '\'') return 1;
    if(lettre == '-') return 1;

    return 0;
}

char * obfuscation(char mot[], int lettresValidees[], int taille) { // pour afficher les trous dans le mot
    int tailleMotCache = taille * sizeof(char) * 2;
    char * motCache = malloc(tailleMotCache); // x2 pour l'espace entre les "_"
    int j = 0;
    for(int i = 0; i < taille; i++) {
        if(lettresValidees[i] != 1) { // pas trouvé
            if(caractereSpecial(mot[i]) == 0) motCache[j] = '_'; // si c'est pas un caractère spéciale
            else { // si c'en est un
                motCache[j] = mot[i];
                lettresValidees[i] = 1;
            }
        } else motCache[j] = mot[i]; // trouvé
        motCache[j + 1] = ' '; // on rajoute un espace entre les underscores
        j = j + 2;
    }
    for(int i = 0; i < longueurMot(motCache); i++)
        if(i >= tailleMotCache) motCache[i] = 0;

    return motCache;
}

int lettreDansMot(char mot[], int tailleMot, char lettre[], int lettresValidees[]) { // pour vérifier si la lettre est dans le mot
    int ok = 0;
    for(int i = 0; i < tailleMot; i++) if(mot[i] == lettre[0]) {
        lettresValidees[i] = 1; // note que la lettre a été découverte
        ok = 1;
    };

    return ok;
}

int partieGagnee(int lettresValidees[], int taille) { // pour vérifier si la partie est gagné
    for(int i = 0; i < taille; i++) if(lettresValidees[i] != 1) return 0;

    return 1;
}

int jeu(const char liste[]) { // déroulement du jeu
    char * mot = recuperationMot(liste); // mot aléatoire
    int tailleMot = longueurMot(mot); // longueur du mot
    int tableauLettresValidees[tailleMot]; // tableau des lettres trouvés par le joueur
    for(int i = 0; i < tailleMot; i++) tableauLettresValidees[i] = 0; // initialisation de tout le tableau à 0
    char * motObfusque; // mot avec les lettres pas encore trouvées cachées
    int demandeLettre; // scanf
    char lettre; // lettre que le joueur propose
    short int essaisRestants = 10; // nombre d'essais
    int finDuJeu = 0; // 0 si partie en cours, 1 si partie terminée

    // lancement jeu
    while(finDuJeu == 0) {
        motObfusque = obfuscation(mot, tableauLettresValidees, tailleMot); // récupération du mot a trou
        printf("\nMot à trouver : %s\n", motObfusque);
        free(motObfusque);
        printf("Nombre d'erreurs restantes : %hu\n", essaisRestants);
        printf("Saisissez une lettre : ");
        demandeLettre = scanf(" %c", &lettre); // demande de la lettre au joueur

        if(demandeLettre == 1) {
            printf("\n");
            if(lettre >= 97 && lettre <= 122) lettre = lettre - 32; // capitalisation de la lettre
            // vérification si lettre dans le mot
            if(lettreDansMot(mot, tailleMot, &lettre, tableauLettresValidees) == 1) { // vérification lettre dans le mot
                printf("La lettre %c est dans le mot !\n", lettre);
                if(partieGagnee(tableauLettresValidees, tailleMot) == 1) { // vérification partie gagnée
                    printf("Le mot est %s\n", mot);
                    return 1;
                }
            } else {
                printf("La lettre %c n'est pas dans le mot !\n", lettre);
                essaisRestants--;
            }
        } else perror("Error");

        if(essaisRestants == 0) finDuJeu = 1; // verification plus d'essais restants
    }
    printf("\nLe mot était %s\n", mot);
    free(mot);

    return 0;
}

int main(const int argc, const char * argv[]) {
    printf("-- Jeu du Pendu --\n");
    if(argc > 1) {
        if(argc > 2) {
            printf("Trop d'arguments renseignés.\n");
            return 0;
        }
        argv++;
        if(jeu(argv[0]) == 1) printf("\\o/ Bravo ! Vous remportez la partie ! \\o/\n");
        else printf("Vous perdez la partie.\n");
    } else printf("Veuillez préciser le dictionnaire à utiliser.\n");

    return 0;
}
